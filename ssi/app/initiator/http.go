package initiator

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"ssi/internal/controller"
	"ssi/internal/database"
	"ssi/internal/model"
	"ssi/internal/repository"
	"ssi/internal/usecase"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/color"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}
func Run() {
	InitConfig()

	//init echo
	r := echo.New()
	r.Validator = &CustomValidator{validator: validator.New()}

	// Config
	log.Println("[INFO] Loading config")
	cfg := NewConfig()

	//fmt.Println(cfg)
	//database postgree
	postgresDB := database.NewPostgresDatabase(cfg)

	//repository
	userRepo := repository.NewUserRepository(postgresDB)

	//
	//usecase
	useCase := usecase.NewUserUseCase(userRepo)

	//controller testing
	userController := controller.NewUserController(useCase)

	//Routing
	log.Println("[INFO] Loging Routing")
	NewRoutes(cfg, r, userController)

	// Server Runner
	log.Println("[INFO] Loading server")
	serverRunner(cfg, r)
}

func serverRunner(cfg model.Config,
	r *echo.Echo) {

	go func() {
		if err := r.Start(fmt.Sprintf("%s:%s", cfg.Host.Address, cfg.Host.Port)); err != nil && err != http.ErrServerClosed {
			r.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 30 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	sig := <-quit
	fmt.Println()
	color.Println(color.Green(fmt.Sprint("received shutdown signal. Trying to shutdown gracefully ", sig)))
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := r.Shutdown(ctx); err != nil {
		log.Fatal("Failure while shutting down gracefully, errApp: ", err)
	}
	color.Println(color.Green("Shutdown gracefully completed"))

}
