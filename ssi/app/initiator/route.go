package initiator

import (
	"net/http"
	"ssi/internal/controller"
	"ssi/internal/model"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func NewRoutes(config model.Config,
	r *echo.Echo,
	userController controller.UserController,
) http.Handler {

	setMiddlewareGlobal(r)

	r.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello Word Riza Nidhom Fahmi")
	})

	r.GET("/user", func(c echo.Context) error {
		return c.String(http.StatusOK, userController.CobaController())
	})

	r.GET("/show-user", userController.ShowUser)
	r.GET("/show-user-byid", userController.ShowUserById)
	r.GET("/show-user-byid/:id", userController.ShowUserById)
	r.POST("/show-user-limit", userController.ShowUserWithLimit)

	return r
}

// setMiddlewareGlobal set middleware global
func setMiddlewareGlobal(r *echo.Echo) {
	// Logger
	r.Use(middleware.Logger())

	r.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"https://*", "http://*"},
		AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization, echo.HeaderXCSRFToken},
		AllowMethods:     []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPut},
		AllowCredentials: false,
		ExposeHeaders:    []string{"Link"},
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	// Recovery
	r.Use(middleware.Recover())
}
