package controller

import (
	"fmt"
	"net/http"
	"ssi/internal/model"
	"ssi/internal/usecase"

	"github.com/labstack/echo/v4"
)

//interface
type UserController interface {
	CobaController() string
	ShowUser(ec echo.Context) error
	ShowUserById(ec echo.Context) error
	ShowUserWithLimit(ec echo.Context) error
}

//implementasi interface
type UserControllerImpl struct {
	UserUC usecase.UserUseCase
}

func NewUserController(UserUC usecase.UserUseCase) UserController {
	return &UserControllerImpl{
		UserUC: UserUC,
	}
}

func (r UserControllerImpl) CobaController() string {
	return "output Testing Coba controller"
}

func (r UserControllerImpl) ShowUser(ec echo.Context) error {
	res := r.UserUC.ShowUser()
	ec.JSON(http.StatusOK, res)
	return nil
}

func (r UserControllerImpl) ShowUserById(ec echo.Context) error {
	ctx := ec.Request().Context()

	requestParam := model.UserRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		// ec.JSON(http.StatusBadRequest, "Param mus be Number")
		return err
	}
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "param Id")
		return err
	}

	id := requestParam.Id
	fmt.Println(id)

	res, err := r.UserUC.ShowUserById(ctx, id)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
	} else {
		fmt.Println("ZZZZZerror")
	}

	if res == nil {
		ec.JSON(http.StatusOK, "Data Not Found")
		return nil
	}

	ec.JSON(http.StatusOK, res)
	return nil
}

func (r UserControllerImpl) ShowUserByLimit(ec echo.Context) error {
	ctx := ec.Request().Context()

	requestParam := model.UserListRequest{}
	if err := ec.Bind(&requestParam); err != nil {
		// ec.JSON(http.StatusBadRequest, "Param mus be Number")
		return err
	}
	if err := ec.Validate(requestParam); err != nil {
		ec.JSON(http.StatusBadRequest, "param Id Required")
		return err
	}

	limit := requestParam.Limit
	// fmt.Println(id)

	res, err := r.UserUC.ShowUserWithLimit(ctx, limit)
	if err != nil {
		ec.JSON(http.StatusInternalServerError, err)
	} else {
		fmt.Println("ZZZZZerror")
	}

	if res == nil {
		ec.JSON(http.StatusOK, "Data Not Found")
		return nil
	}

	ec.JSON(http.StatusOK, res)
	return nil
}
