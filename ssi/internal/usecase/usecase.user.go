package usecase

import (
	"context"
	// "log"
	"ssi/internal/model"
	"ssi/internal/repository"
)

type UserUseCase interface {
	ShowUser() (res model.UserRespone)
	ShowUserById(ctx context.Context, id int64) (res *model.UserRespone, err error)
	ShowUserWithLimit(ctx context.Context, limit int64) (res []model.UserRespone, err error)
}

type UserUseCaseImpl struct {
	userRepo repository.UserRepository
}

func NewUserUseCase(userRepo repository.UserRepository) UserUseCase {
	return UserUseCaseImpl{
		userRepo: userRepo,
	}
}

func (uc UserUseCaseImpl) ShowUser() (res model.UserRespone) {
	res.Id = 1
	res.Nama = "Riza Nidhom"

	return res
}

func (uc UserUseCaseImpl) ShowUserById(ctx context.Context, id int64) (res *model.UserRespone, err error) {
	resultData, err := uc.userRepo.GetdataByid(ctx, id)
	if err != nil {
		//log.Errorf("errroorr bro :%v", err)
		return res, err
	}
	if resultData == nil {
		return res, err
	}

	resMap := mapDatauserrespone(*resultData)
	return &resMap, err
}

func mapDatauserrespone(reg model.UserCobaModel) (result model.UserRespone) {
	result.Id = reg.Id
	result.Nama = reg.Nama.String
	return result
}

func (uc UserUseCaseImpl) ShowUserWithLimit(ctx context.Context, limit int64) (res *model.UserRespone, err error) {
	resultData, err := uc.userRepo.GetDataUserByLimit(ctx, limit)
	if err != nil {
		//log.Errorf("errroorr bro :%v", err)
		return res, err
	}
	if resultData == nil {
		return res, err
	}

	resMap := mapListDatauserrespone(resultData)
	return resMap, err
}

func mapListDatauserrespone(reg []model.UserCobaModel) (result []model.UserRespone) {
	for _, v := range reg {
		row := model.UserRespone{
			Id:   v.Id,
			Nama: v.Nama.String,
		}
		result = append(result, row)

	}
	return result
}
