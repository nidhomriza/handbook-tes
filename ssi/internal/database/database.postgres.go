package database

import (
	"fmt"
	"log"

	"ssi/internal/model"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type (
	// SqlxConfig config
	SqlxConfig struct {
		Driver, Host, Port, Username, Password, DatabaseName string
	}
)

func Newdatabase(config SqlxConfig) *sqlx.DB {
	var stringConnection string
	if config.Driver == "postgres" {
		stringConnection = fmt.Sprintf("port=%v host=%s user=%s password=%s dbname=%s sslmode=disable", config.Port, config.Host, config.Username, config.Password, config.DatabaseName)
	} else {
		stringConnection = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", config.Username, config.Password, config.Host, config.Port, config.DatabaseName)
	}
	//fmt.Println(stringConnection)

	db, err := sqlx.Connect(config.Driver, stringConnection)
	if err != nil {
		log.Fatalf("error when sqlx.Connect, error: %v", err.Error())
	} else {
		fmt.Println("Koneksi Berhasil")
	}

	if err := db.Ping(); err != nil {
		log.Fatalf("error when ping %s, error: %v", config.Driver, err.Error())
	}

	return db
}

// PostgresDatabase
type PostgresDatabase struct {
	PostgresMaster *sqlx.DB
	PostgresSlave  *sqlx.DB
}

// PostgresDatabase
func NewPostgresDatabase(config model.Config) PostgresDatabase {

	//fmt.Println(config)

	postgresDBMaster := Newdatabase(SqlxConfig{
		Driver:       config.Database.PostgressMaster.Driver,
		Host:         config.Database.PostgressMaster.Host,
		Port:         config.Database.PostgressMaster.Port,
		Username:     config.Database.PostgressMaster.Username,
		Password:     config.Database.PostgressMaster.Password,
		DatabaseName: config.Database.PostgressMaster.Database,
	})

	postgresDBSlave := Newdatabase(SqlxConfig{
		Driver:       config.Database.PostgressSlave.Driver,
		Host:         config.Database.PostgressSlave.Host,
		Port:         config.Database.PostgressSlave.Port,
		Username:     config.Database.PostgressSlave.Username,
		Password:     config.Database.PostgressSlave.Password,
		DatabaseName: config.Database.PostgressSlave.Database,
	})

	return PostgresDatabase{
		PostgresMaster: postgresDBMaster,
		PostgresSlave:  postgresDBSlave,
	}
}
