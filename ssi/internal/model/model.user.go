package model

import "database/sql"

type UserRespone struct {
	Id   int64  `json:"id"`
	Nama string `json:"nama"`
}

type UserCobaModel struct {
	Id   int64          `json:"id"`
	Nama sql.NullString `json:"nama"`
}

type UserRequest struct {
	Id int64 `param:"id" query:"id" json:"id" validate:"required,number"`
}

type UserListRequest struct {
	limit int64 `form:"limit" json:"limit" validate:"required,number"`
}
