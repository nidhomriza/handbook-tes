package model

type Config struct {
	Env      string      `mapstructure:"env"`
	Host     Host        `mapstructure:"host"`
	Database Database    `mapstructure:"database"`
	Redis    Redis       `mapstructure:"redis"`
	Mongo    MongoConfig `mapstructure:"mongo"`
	Cron     Cronservice `mapstructure:"cron"`
}

type Database struct {
	PostgressMaster DatabaseSource `mapstructure:"postgress_master"`
	PostgressSlave  DatabaseSource `mapstructure:"postgress_slave"`
}

type DatabaseSource struct {
	Driver   string `mapstructure:"driver"`
	Host     string `mapstructure:"host"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	Database string `mapstructure:"database"`
	Port     string `mapstructure:"port"`
}

type Redis struct {
	Address   string `mapstructure:"address"`
	Database  string `mapstructure:"database"`
	Password  string `mapstructure:"password"`
	Timeout   string `mapstructure:"timeout"`
	MaxActive string `mapstructure:"maxactive"`
}

type Host struct {
	Address      string `mapsstructure:"address"`
	Port         string `mapsstructure:"port"`
	WriteTimeout int    `mapstructure:"write_timeout" default:"15"`
	ReadTimeout  int    `mapstructure:"read_timeout" default:"15"`
	IdleTimeout  int    `mapstructure:"idle_timeout" default:"60"`
}

type MongoConfig struct {
	DSN                string `mapstructure:"dsn"`
	MaxPoolConnections int    `mapstructure:"max_pool_conns"`
	DatabaseLog        string `mapstructure:"database_log"`
}

type Cronservice struct {
	IndexingReport string `mapstructure:"indexing_report"`
}
