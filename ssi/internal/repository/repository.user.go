package repository

import (
	"context"
	"database/sql"
	"ssi/internal/database"
	"ssi/internal/model"

	"github.com/labstack/gommon/log"
)

type UserRepository interface {
	GetdataByid(ctx context.Context, id int64) (result *model.UserCobaModel, err error)
	GetDataUserByLimit(ctx context.Context, limit int64) (result []model.UserCobaModel, err error)
}

type UserRepositoryImpl struct {
	postgresDB database.PostgresDatabase
}

func NewUserRepository(postgresDB database.PostgresDatabase) UserRepository {
	return UserRepositoryImpl{
		postgresDB: postgresDB,
	}
}

func (r UserRepositoryImpl) GetdataByid(ctx context.Context, id int64) (result *model.UserCobaModel, err error) {
	db := r.postgresDB.PostgresSlave
	userData := model.UserCobaModel{}

	query := "select id, nama from user_coba where id=$1 limit 1"

	err = db.QueryRowxContext(ctx, query, id).StructScan(&userData)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Errorf("r.db.QueryRowxContext GetToken err: %v", err)
		return &userData, err
	}
	return &userData, err
}

func (r UserRepositoryImpl) GetDataUserByLimit(ctx context.Context, limit int64) (result []model.Database, err error) {
	db := r.postgresDB.PostgresSlave

	query := "select id, nama from user_coba limit $1 "

	//query scan
	rows, err := db.QueryxContext(ctx, query, limit)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return result, err
	}

	for rows.Next() {
		userData := model.UserCobaModel{}

		err := rows.StructScan(&userData)
		if err != nil {
			return result, err
		}

		result = append(result, userData)

	}

	defer func() {
		if err = rows.Close(); err != nil {
			return
		}

	}()

	return result, err
}
